import locales from "./locales";
import zh_CN from "./zh_CN"; // default locale

const checkLocale = () => {
  if (typeof window !== "undefined") {
    let language = window.localStorage.getItem("language");
    let userLanguage = navigator.language.replace("-", "_");
    return language ? language : locales[userLanguage] ? userLanguage : "zh_CN";
  }
};

const getTranslations = (key, locale = checkLocale()) => {
  const currLocale = locales[locale] ? locales[locale] : zh_CN;
  let translated = currLocale[key] ? currLocale[key] : zh_CN[key];
  return translated;
};

export { getTranslations, checkLocale };
