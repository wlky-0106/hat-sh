import zh_CN from "./zh_CN";

const locales = {
  zh_CN
};

export default locales;
